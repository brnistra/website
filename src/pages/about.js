import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const SecondPage = () => (
  <Layout>
    <SEO title="About us" />
    <h1>About us</h1>
    <p>We are a team of IT experts dedicated to building fast, reliable and innovative websites and tech solutions.</p>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export default SecondPage
